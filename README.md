<div align="center">
<h1 align="center">
  <img src="https://gitlab.com/uploads/-/system/project/avatar/18692209/ad0a03db14ec7e62dd54ac5821ae568240f4.png?width=88" alt="Project">
  <br />
  ChromeOS Themes for Snap
</h1>
</div>

<p align="center"><b>These are the ChromeOS GTK themes for snaps.</b> ChromeOS is a Material Design theme for GNOME/GTK based desktop environments

This theme is based on the <a href="https://github.com/nana-4/materia-theme">Materia theme</a> of nana-4. 

If you are using the Vimix icon theme along with the ChromeOS theme, you need to install vimix from the Snap Store in order to see the app icons in the store properly. Please run: 

```
sudo snap install vimix-themes
```

<b>Attributions:</b>
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/ChromeOS-theme">ChromeOS-theme</a>. The ChromeOS theme is under a GPL3.0 licence.

</p>


## Install
<a href="https://snapcraft.io/chromeos-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the snap from the Snap Store оr by running:
```
sudo snap install chromeos-themes
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes chromeos-themes:gtk-3-themes 
```
```
sudo snap connect [other snap]:icon-themes chromeos-themes:icon-themes
```

## Icons
If you are using the Vimix icon theme along with the ChromeOS theme, you need to install ```vimix-themes``` from the Snap Store in order to see the app icons in the store properly. Please run:

```
sudo snap install vimix-themes
```
